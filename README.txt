For more information please visit http://drupal.org/node/456

CONTENTS OF THIS FILE
---------------------

 * About Drupal
 * About eClean theme
   - Specifications
   - Observations
 * Installing Drupal Themes
 

*** About Drupal
------------
Drupal is an open source content management platform supporting a variety of
websites ranging from personal weblogs to large community-driven websites. For
more information, see the Drupal website at http://drupal.org/, and join the
Drupal community at http://drupal.org/community.

Legal information about Drupal:
 * Know your rights when using Drupal:
   See LICENSE.txt in the same directory as this document.
 * Learn about the Drupal trademark and logo policy:
   http://drupal.com/trademark


*** About eClean theme
---------------------------------------
The theme is intended for various types of sites. The main characteristics of eClean are responsivity and that
it is built with HTML5 and CSS3. The layout is multi-column with fluid width and tableless.

- Specifications
  1. Max-width: 1140px.
  2. Min-width: 335px.
  3. Responsivity width steps:  920px
                                780px
                                420px (smart phones)
  4. Works fine on: Chrome
                    Firefox 9+
                    Opera 11+
                    Safari 5+
  5. JavaScript is required for reordering sidebars at width < 920px;

- Observations
  1. eClean is not yet tested in Internet Explorer.
  2. When the RDF module is enabled, the doctype is changed to HTML+RDF. The W3C Validator is not compatible with
     this doctype and returns a lot of erros.


*** Installing Drupal Themes 
---------------------------------------
1. Access your Web server using an FTP client or Web server administration tools.
2. Create a folder for your specific theme under "<YourSiteFolder>/sites/all/themes/" folder within Drupal installation.
   For example: <YourSiteFolder>/sites/all/themes/<MyNewTheme>
3. Copy or upload theme files into the newly created <MyNewTheme> folder.
4. Login to your Drupal Administer.
5. Go to Drupal Administer -> Appearance (www.YourSite.com/?q=admin/appearance)
6. In the bottom of the page you will find the theme you've downloaded to the location of your site.
7. Click "Enable and set default" below the theme.
For more information please visit: http://drupal.org/node/456


---------------------------------------
by Dhavyd Vanderlei - http://www.dhavyd.com